EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev "v1.0.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CO2Mofetten:GSS_ExplorIR_M_CO2 U1
U 1 1 5F3CF0A4
P 6100 3800
F 0 "U1" H 6418 3896 50  0000 L CNN
F 1 "GSS_ExplorIR_M_CO2" H 6418 3805 50  0000 L CNN
F 2 "CO2Mofetten:GSS_ExplorIR_M_CO2_PinSocket" H 6100 4250 50  0001 C CNN
F 3 "" H 6100 4250 50  0001 C CNN
	1    6100 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5F3CFBD2
P 5700 4350
F 0 "#PWR0101" H 5700 4100 50  0001 C CNN
F 1 "GND" H 5705 4177 50  0000 C CNN
F 2 "" H 5700 4350 50  0001 C CNN
F 3 "" H 5700 4350 50  0001 C CNN
	1    5700 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3800 5950 3800
$Comp
L power:+5V #PWR0102
U 1 1 5F3CFF21
P 5750 2900
F 0 "#PWR0102" H 5750 2750 50  0001 C CNN
F 1 "+5V" H 5765 3073 50  0000 C CNN
F 2 "" H 5750 2900 50  0001 C CNN
F 3 "" H 5750 2900 50  0001 C CNN
	1    5750 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 4350 5700 4300
Wire Wire Line
	5750 2900 5750 3950
Wire Wire Line
	5750 3950 5950 3950
Wire Wire Line
	4950 4000 5750 4000
Wire Wire Line
	5750 4000 5750 3950
Connection ~ 5750 3950
Wire Wire Line
	4650 4200 4750 4200
Connection ~ 4650 4200
Connection ~ 4450 4200
Wire Wire Line
	4450 4200 4550 4200
Wire Wire Line
	4350 4200 4450 4200
$Comp
L CO2Mofetten:RJ45SensorCableConnector J1
U 1 1 5F3D0991
P 4550 3700
F 0 "J1" H 4607 4367 50  0000 C CNN
F 1 "RJ45SensorCableConnector" H 4607 4276 50  0000 C CNN
F 2 "CO2Mofetten:RJ45_econ_MEB8.8PG_Horizontal" V 4550 3725 50  0001 C CNN
F 3 "~" V 4550 3725 50  0001 C CNN
	1    4550 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 3700 5000 3700
Wire Wire Line
	5000 3700 5000 3400
Wire Wire Line
	5000 3400 4950 3400
Wire Wire Line
	4950 3300 4950 3400
Connection ~ 4950 3400
Wire Wire Line
	4950 3400 4950 3500
$Comp
L power:+3V3 #PWR0103
U 1 1 5F3D237A
P 5450 2950
F 0 "#PWR0103" H 5450 2800 50  0001 C CNN
F 1 "+3V3" H 5465 3123 50  0000 C CNN
F 2 "" H 5450 2950 50  0001 C CNN
F 3 "" H 5450 2950 50  0001 C CNN
	1    5450 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2950 5450 3900
Wire Wire Line
	5450 3900 4950 3900
Wire Wire Line
	4950 3600 5550 3600
Wire Wire Line
	5550 3600 5550 3650
Wire Wire Line
	5550 3650 5950 3650
Wire Wire Line
	5100 3500 5100 3800
Wire Wire Line
	5100 3800 4950 3800
Wire Wire Line
	5100 3500 5950 3500
Wire Wire Line
	4550 4200 4550 4300
Wire Wire Line
	4550 4300 5000 4300
Connection ~ 4550 4200
Wire Wire Line
	4550 4200 4650 4200
Connection ~ 5700 4300
Wire Wire Line
	5700 4300 5700 3800
Wire Wire Line
	5000 3700 5000 4300
Connection ~ 5000 3700
Connection ~ 5000 4300
Wire Wire Line
	5000 4300 5700 4300
$EndSCHEMATC
